/*********************************************************************
* Copyright (c) 2005, 2019 SAP SE
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Contributors:
*    SAP SE - initial API, implementation and documentation
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipse.graphiti.examples.tutorial;

import org.eclipse.graphiti.mm.pictograms.PictogramElement;
import org.eclipse.graphiti.services.Graphiti;

public class PropertyUtil {

	public static final String SHAPE_KEY = "shape-id"; //$NON-NLS-1$

	public static final String SHAPE_VALUE_E_CLASS = "e-class"; //$NON-NLS-1$

	public static final void setEClassShape(PictogramElement pe) {
		Graphiti.getPeService().setPropertyValue(pe, SHAPE_KEY, SHAPE_VALUE_E_CLASS);
	}

	public static boolean isEClassShape(PictogramElement pe) {
		return SHAPE_VALUE_E_CLASS.equals(Graphiti.getPeService().getPropertyValue(pe, SHAPE_KEY));
	}
}
