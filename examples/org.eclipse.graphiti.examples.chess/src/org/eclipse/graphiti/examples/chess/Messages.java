/*********************************************************************
* Copyright (c) 2011, 2019 SAP SE
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Contributors:
*    SAP SE - initial API, implementation and documentation
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipse.graphiti.examples.chess;

import org.eclipse.osgi.util.NLS;

/**
 *
 */
public class Messages extends NLS {
	private static final String BUNDLE_NAME = "org.eclipse.graphiti.examples.chess.messages"; //$NON-NLS-1$
	public static String CreateAllInitialChessPiecesFeature_name;
	public static String CreateAllInitialChessPiecesFeature_description;
	public static String CreateChessBoardFeature_name;
	public static String CreateChessBoardFeature_description;
	public static String CreateChessMoveFeature_name;
	public static String CreateChessMoveFeature_description;
	static {
		// initialize resource bundle
		NLS.initializeMessages(BUNDLE_NAME, Messages.class);
	}

	private Messages() {
	}
}
