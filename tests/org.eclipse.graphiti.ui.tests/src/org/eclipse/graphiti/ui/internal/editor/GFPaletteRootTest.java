/*********************************************************************
* Copyright (c) 2015, 2019 SAP SE
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Contributors:
*    mwenz - Bug 467476 - NullPointerException in GFPaletteRoot.createModelIndependentTools
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipse.graphiti.ui.internal.editor;

import static org.junit.Assert.fail;

import org.junit.Test;

public class GFPaletteRootTest {

	@SuppressWarnings("restriction")
	@Test
	public void testGFPaletteRoot() {
		try {
			new GFPaletteRoot(null);
			fail("IllegalArgumentException was expected");
		} catch (IllegalArgumentException e) {
			// expected exception
		}
	}
}
