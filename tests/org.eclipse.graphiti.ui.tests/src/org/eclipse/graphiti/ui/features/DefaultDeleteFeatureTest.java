/*********************************************************************
* Copyright (c) 2014, 2022 SAP SE
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Contributors:
*    mwenz - Bug 453553 - Provide an abort possibility for delete and remove features in case 'pre' methods fail
*    mwenz - Bug 580392 - Replace EasyMock with Mockito
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipse.graphiti.ui.features;

import static org.junit.Assert.fail;
import static org.mockito.Mockito.mock;

import org.eclipse.core.runtime.OperationCanceledException;
import org.eclipse.graphiti.features.IFeature;
import org.eclipse.graphiti.features.IFeatureProvider;
import org.eclipse.graphiti.features.context.IContext;
import org.eclipse.graphiti.features.context.IDeleteContext;
import org.junit.Test;

public class DefaultDeleteFeatureTest {

	@Test
	public void testIsAbortAfterPreDelete() {
		IContext context = mock(IDeleteContext.class);
		IFeatureProvider featureProvider = mock(IFeatureProvider.class);

		IFeature feature = new TestDeleteFeatureAbortOnFirstCallToIsAbort(featureProvider);

		try {
			feature.execute(context);
			fail("Must not be reached");
		} catch (OperationCanceledException e) {
			// expected
		}
	}

	private class TestDeleteFeatureAbortOnFirstCallToIsAbort extends DefaultDeleteFeature {

		public TestDeleteFeatureAbortOnFirstCallToIsAbort(IFeatureProvider fp) {
			super(fp);
		}

		@Override
		public boolean canDelete(IDeleteContext context) {
			return true;
		}

		@Override
		public boolean isDeleteAbort() {
			return true;
		}

		@Override
		protected void deleteBusinessObjects(Object[] businessObjects) {
			super.deleteBusinessObjects(businessObjects);
			fail("Must not be reached");
		}

		@Override
		public void delete(IDeleteContext context) {
			super.delete(context);
		}

		@Override
		public boolean hasDoneChanges() {
			return false;
		}
	}
}
