/*********************************************************************
* Copyright (c) 2012, 2022 SAP SE
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Contributors:
*    SAP SE - initial API, implementation and documentation
*    mlypik - Bug 401792 - Disable starting reconnection
*    mwenz - Bug 580392 - Replace EasyMock with Mockito
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipse.graphiti.ui.tests;

import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Arrays;

import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.graphiti.dt.IDiagramTypeProvider;
import org.eclipse.graphiti.features.IAddFeature;
import org.eclipse.graphiti.features.ICreateConnectionFeature;
import org.eclipse.graphiti.features.ICreateFeature;
import org.eclipse.graphiti.features.IFeature;
import org.eclipse.graphiti.features.IFeatureProvider;
import org.eclipse.graphiti.features.IReconnectionFeature;
import org.eclipse.graphiti.features.context.IAddContext;
import org.eclipse.graphiti.features.context.IContext;
import org.eclipse.graphiti.features.context.ICreateConnectionContext;
import org.eclipse.graphiti.features.context.ICreateContext;
import org.eclipse.graphiti.features.context.IReconnectionContext;
import org.eclipse.graphiti.mm.pictograms.Anchor;
import org.eclipse.graphiti.mm.pictograms.Connection;
import org.eclipse.graphiti.mm.pictograms.ContainerShape;
import org.eclipse.graphiti.mm.pictograms.PictogramElement;
import org.eclipse.graphiti.tests.reuse.GFAbstractTestCase;
import org.eclipse.graphiti.ui.internal.command.AddModelObjectCommand;
import org.eclipse.graphiti.ui.internal.command.CreateConnectionCommand;
import org.eclipse.graphiti.ui.internal.command.CreateModelObjectCommand;
import org.eclipse.graphiti.ui.internal.command.ReconnectCommand;
import org.eclipse.graphiti.ui.internal.config.IConfigurationProviderInternal;
import org.eclipse.graphiti.ui.platform.IConfigurationProvider;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.StructuredSelection;
import org.junit.Test;

public class CommandTest extends GFAbstractTestCase {

	@Test
	public void testAddModelObjectCommand_canUndo_callsFeature() throws Exception {
		final boolean[] undoWasCalled = { false };

		IAddFeature addFeature = new IAddFeature() {
			@Override
			public IFeatureProvider getFeatureProvider() {
				return null;
			}

			@Override
			public String getDescription() {
				return null;
			}

			@Override
			public String getName() {
				return null;
			}

			@Override
			public boolean isAvailable(IContext context) {
				return false;
			}

			@Override
			public boolean hasDoneChanges() {
				return false;
			}

			@Override
			public void execute(IContext context) {
			}

			@Override
			public boolean canUndo(IContext context) {
				undoWasCalled[0] = true;
				return true;
			}

			@Override
			public boolean canExecute(IContext context) {
				return false;
			}

			@Override
			public boolean canAdd(IAddContext context) {
				return true;
			}

			@Override
			public PictogramElement add(IAddContext context) {
				return null;
			}
		};

		IFeatureProvider featureProviderMock = mock(IFeatureProvider.class);
		when(featureProviderMock.getAddFeature((IAddContext) any())).thenReturn(addFeature);

		IDiagramTypeProvider diagramTypeProviderMock = mock(IDiagramTypeProvider.class);
		when(diagramTypeProviderMock.getFeatureProvider()).thenReturn(featureProviderMock);

		IConfigurationProviderInternal configurationProviderMock = mock(IConfigurationProviderInternal.class);
		when(configurationProviderMock.getDiagramTypeProvider()).thenReturn(diagramTypeProviderMock);

		ContainerShape containerShapeMock = mock(ContainerShape.class);

		IAdaptable adaptableMock = mock(IAdaptable.class);
		when(adaptableMock.getAdapter(EObject.class)).thenReturn(containerShapeMock);

		ISelection selection = new StructuredSelection(new Object[] { adaptableMock });

		Rectangle rectangle = new Rectangle();

		AddModelObjectCommand myAddModelObjectCommand = new AddModelObjectCommand(configurationProviderMock,
				containerShapeMock, selection, rectangle);

		assertTrue(myAddModelObjectCommand.canUndo());

		assertTrue(undoWasCalled[0]);
	}

	@Test
	public void testCreateConnectionCommand_canUndo_callsFeature() throws Exception {
		final boolean[] undoWasCalled = { false };

		ICreateConnectionFeature[] createConnectionFeatures = { new ICreateConnectionFeature() {
			@Override
			public boolean canUndo(IContext context) {
				undoWasCalled[0] = true;
				return true;
			}

			@Override
			public boolean canCreate(ICreateConnectionContext context) {
				return false;
			}

			@Override
			public Connection create(ICreateConnectionContext context) {
				return null;
			}

			@Override
			public boolean canStartConnection(ICreateConnectionContext context) {
				return false;
			}

			@Override
			public void startConnecting() {
			}

			@Override
			public void endConnecting() {
			}

			@Override
			public void attachedToSource(ICreateConnectionContext context) {
			}

			@Override
			public void canceledAttaching(ICreateConnectionContext context) {
			}

			@Override
			public String getCreateName() {
				return null;
			}

			@Override
			public String getCreateDescription() {
				return null;
			}

			@Override
			public String getCreateImageId() {
				return null;
			}

			@Override
			public String getCreateLargeImageId() {
				return null;
			}

			@Override
			public boolean isAvailable(IContext context) {
				return false;
			}

			@Override
			public boolean canExecute(IContext context) {
				return false;
			}

			@Override
			public void execute(IContext context) {
			}

			@Override
			public boolean hasDoneChanges() {
				return false;
			}

			@Override
			public String getName() {
				return null;
			}

			@Override
			public String getDescription() {
				return null;
			}

			@Override
			public IFeatureProvider getFeatureProvider() {
				return null;
			}
		}};

		IFeatureProvider featureProviderMock = mock(IFeatureProvider.class);
		when(featureProviderMock.getCreateConnectionFeatures()).thenReturn(
				(ICreateConnectionFeature[]) createConnectionFeatures);

		IDiagramTypeProvider diagramTypeProviderMock = mock(IDiagramTypeProvider.class);
		when(diagramTypeProviderMock.getFeatureProvider()).thenReturn(featureProviderMock);

		IConfigurationProvider configurationProviderMock = mock(IConfigurationProvider.class);
		when(configurationProviderMock.getDiagramTypeProvider()).thenReturn(diagramTypeProviderMock);

		@SuppressWarnings("unchecked")
		EList<Anchor> anchorList = mock(EList.class);
		when(anchorList.iterator()).thenReturn(new ArrayList().iterator());
		
		ContainerShape containerShapeMock = mock(ContainerShape.class);
		when(containerShapeMock.getAnchors()).thenReturn(anchorList);

		IFeature[] features = createConnectionFeatures;
		CreateConnectionCommand myCreateConnectionCommand = new CreateConnectionCommand(configurationProviderMock,
				containerShapeMock, Arrays.asList(features));

		assertTrue(myCreateConnectionCommand.canUndo());

		assertTrue(undoWasCalled[0]);
	}

	@Test
	public void testCreateodelObjectCommand_canUndo_callsFeature() throws Exception {
		final boolean[] undoWasCalled = { false };

		ICreateFeature[] createFeatures = { new ICreateFeature() {
			@Override
			public boolean canUndo(IContext context) {
				undoWasCalled[0] = true;
				return true;
			}

			@Override
			public boolean canCreate(ICreateContext context) {
				return false;
			}

			@Override
			public Object[] create(ICreateContext context) {
				return null;
			}

			@Override
			public String getCreateName() {
				return null;
			}

			@Override
			public String getCreateDescription() {
				return null;
			}

			@Override
			public String getCreateImageId() {
				return null;
			}

			@Override
			public String getCreateLargeImageId() {
				return null;
			}

			@Override
			public boolean isAvailable(IContext context) {
				return false;
			}

			@Override
			public boolean canExecute(IContext context) {
				return false;
			}

			@Override
			public void execute(IContext context) {
			}

			@Override
			public boolean hasDoneChanges() {
				return false;
			}

			@Override
			public String getName() {
				return null;
			}

			@Override
			public String getDescription() {
				return null;
			}

			@Override
			public IFeatureProvider getFeatureProvider() {
				return null;
			}
		} };

		IFeatureProvider featureProviderMock = mock(IFeatureProvider.class);
		when(featureProviderMock.getCreateFeatures()).thenReturn(createFeatures);

		IDiagramTypeProvider diagramTypeProviderMock = mock(IDiagramTypeProvider.class);
		when(diagramTypeProviderMock.getFeatureProvider()).thenReturn(featureProviderMock);

		IConfigurationProviderInternal configurationProviderMock = mock(IConfigurationProviderInternal.class);
		when(configurationProviderMock.getDiagramTypeProvider()).thenReturn(diagramTypeProviderMock);

		CreateModelObjectCommand myCreateModelObjectCommand = new CreateModelObjectCommand(configurationProviderMock,
				createFeatures[0], null);

		assertTrue(myCreateModelObjectCommand.canUndo());

		assertTrue(undoWasCalled[0]);
	}

	@Test
	public void testReconnectCommand_canUndo_callsFeature() throws Exception {
		final boolean[] undoWasCalled = { false };

		IReconnectionFeature reconnectFeature = new IReconnectionFeature() {

			@Override
			public IFeatureProvider getFeatureProvider() {
				return null;
			}

			@Override
			public String getDescription() {
				return null;
			}

			@Override
			public String getName() {
				return null;
			}

			@Override
			public boolean isAvailable(IContext context) {
				return false;
			}

			@Override
			public boolean hasDoneChanges() {
				return false;
			}

			@Override
			public void execute(IContext context) {
			}

			@Override
			public boolean canUndo(IContext context) {
				undoWasCalled[0] = true;
				return true;
			}

			@Override
			public boolean canExecute(IContext context) {
				return false;
			}

			@Override
			public void reconnect(IReconnectionContext context) {
			}

			@Override
			public void preReconnect(IReconnectionContext context) {
			}

			@Override
			public void postReconnect(IReconnectionContext context) {
			}

			@Override
			public void canceledReconnect(IReconnectionContext context) {
			}

			@Override
			public boolean canReconnect(IReconnectionContext context) {
				return false;
			}

			@Override
			public boolean canStartReconnect(IReconnectionContext context) {
				return false;
			}
		};

		IFeatureProvider featureProviderMock = mock(IFeatureProvider.class);
		when(featureProviderMock.getReconnectionFeature((IReconnectionContext) any()))
				.thenReturn(reconnectFeature);

		IDiagramTypeProvider diagramTypeProviderMock = mock(IDiagramTypeProvider.class);
		when(diagramTypeProviderMock.getFeatureProvider()).thenReturn(featureProviderMock);

		IConfigurationProviderInternal configurationProviderMock = mock(IConfigurationProviderInternal.class);
		when(configurationProviderMock.getDiagramTypeProvider()).thenReturn(diagramTypeProviderMock);

		ReconnectCommand myReconnectCommand = new ReconnectCommand(configurationProviderMock, null, null, null, null,
				null, null);

		assertTrue(myReconnectCommand.canUndo());

		assertTrue(undoWasCalled[0]);
	}
}
