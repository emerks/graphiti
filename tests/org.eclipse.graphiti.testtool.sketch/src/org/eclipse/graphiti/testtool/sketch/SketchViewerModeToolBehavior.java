/*********************************************************************
* Copyright (c) 2005, 2019 SAP SE
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Contributors:
*    SAP SE - initial API, implementation and documentation
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipse.graphiti.testtool.sketch;

import org.eclipse.graphiti.dt.IDiagramTypeProvider;
import org.eclipse.graphiti.features.IFeatureChecker;
import org.eclipse.graphiti.palette.IPaletteCompartmentEntry;

public class SketchViewerModeToolBehavior extends SketchToolBehavior {

	private static final double[] ZOOM_LEVELS = new double[] { 0.01, 0.025, 0.05, 0.1, 0.25, 0.5, 1, 1.5, 2, 3, 4, 5 };
	private IFeatureChecker featureChecker;

	public SketchViewerModeToolBehavior(IDiagramTypeProvider diagramTypeProvider) {
		super(diagramTypeProvider);
	}

	@Override
	protected boolean showCreateConnectionFeatures() {
		return false;
	}

	@Override
	public IPaletteCompartmentEntry[] getPalette() {
		return new IPaletteCompartmentEntry[0];
	}

	@Override
	public double[] getZoomLevels() {
		return ZOOM_LEVELS;
	}

	public IFeatureChecker getFeatureChecker() {
		if (featureChecker == null) {
			featureChecker = new SketchViewerModeChecker();

		}
		return featureChecker;
	}
}
