/*********************************************************************
* Copyright (c) 2005, 2019 SAP SE
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Contributors:
*    SAP SE - initial API, implementation and documentation
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipse.graphiti.bot.pageobjects;

import org.eclipse.swtbot.eclipse.gef.finder.SWTGefBot;

/**
 * Encapsulates SWTBot access and technical details, thereby enabling
 * to write semantic tests, that is, tests were the test flow is more readable
 * and technicalities are hidden.
 *
 */
public abstract class PageObject {

	protected SWTGefBot bot = new SWTGefBot();

}
