/*********************************************************************
* Copyright (c) 2005, 2019 SAP SE and others
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Contributors:
*    SAP SE - initial API, implementation and documentation
*    mgorning - Bug 391523 - Revise getSelectionInfo...() in IToolBehaviorProvider
*    gergo - Gitlab issue 5 - Line width of the selection rectangle cannot be set
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipse.graphiti.tb;

import org.eclipse.graphiti.mm.algorithms.styles.LineStyle;
import org.eclipse.graphiti.util.IColorConstant;

/**
 * The Class SelectionInfoImpl.
 */
public abstract class SelectionInfoImpl implements ISelectionInfo {

	private IColorConstant color;

	private IColorConstant hoverColor;

	private LineStyle lineStyle;

	private int lineWidth = 1;

	private IColorConstant hoverColorParentSelected;

	/**
	 * Creates a new {@link SelectionInfoImpl}.
	 */
	public SelectionInfoImpl() {
	}

	public IColorConstant getColor() {
		return this.color;
	}

	public LineStyle getLineStyle() {
		return this.lineStyle;
	}

	public int getLineWidth() {
		return this.lineWidth;
	}

	public void setLineStyle(LineStyle lineStyle) {
		this.lineStyle = lineStyle;
	}

	public void setColor(IColorConstant color) {
		this.color = color;
	}

	public IColorConstant getHoverColor() {
		return this.hoverColor;
	}

	public void setHoverColor(IColorConstant hoverColor) {
		this.hoverColor = hoverColor;

	}

	public IColorConstant getHoverColorParentSelected() {
		return this.hoverColorParentSelected;
	}

	public void setHoverColorParentSelected(IColorConstant hoverColor) {
		this.hoverColorParentSelected = hoverColor;
	}

	public void setLineWidth(int lineWidth) {
		this.lineWidth = lineWidth;
	}

}
