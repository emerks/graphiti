/*********************************************************************
* Copyright (c) 2005, 2019 SAP SE
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Contributors:
*    SAP SE - initial API, implementation and documentation
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipse.graphiti.palette;

import java.util.List;

import org.eclipse.graphiti.features.IFeature;


/**
 * The Interface IConnectionCreationToolEntry.
 */
public interface IConnectionCreationToolEntry extends ICreationToolEntry {

	/**
	 * Gets the creates the connection features.
	 * 
	 * @return the the List<IFeature> connection features
	 */
	public List<IFeature> getCreateConnectionFeatures();
}
