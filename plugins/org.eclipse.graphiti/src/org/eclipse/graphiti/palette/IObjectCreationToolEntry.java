/*********************************************************************
* Copyright (c) 2005, 2019 SAP SE
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Contributors:
*    SAP SE - initial API, implementation and documentation
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipse.graphiti.palette;

import org.eclipse.graphiti.features.ICreateFeature;

/**
 * The Interface IObjectCreationToolEntry.
 */
public interface IObjectCreationToolEntry extends ICreationToolEntry {

	/**
	 * Gets the creates the feature.
	 * 
	 * @return the ICreateFeature feature
	 */
	public ICreateFeature getCreateFeature();
}
