/*********************************************************************
* Copyright (c) 2005, 2019 SAP SE
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Contributors:
*    SAP SE - initial API, implementation and documentation
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipse.graphiti.features;

import org.eclipse.graphiti.features.impl.AbstractAddFeature;
import org.eclipse.graphiti.func.IAdd;

/**
 * The Interface IAddFeature. A pictogram element has to be added. This
 * pictogram element has to link to a domain model element.
 * 
 * @noextend This interface is not intended to be extended by clients.
 * @noimplement This interface is not intended to be implemented by clients,
 *              extend {@link AbstractAddFeature} instead.
 */
public interface IAddFeature extends IAdd, IFeature {
}