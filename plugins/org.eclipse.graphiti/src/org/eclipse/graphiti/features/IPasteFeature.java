/*********************************************************************
* Copyright (c) 2005, 2019 SAP SE
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Contributors:
*    SAP SE - initial API, implementation and documentation
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipse.graphiti.features;

import org.eclipse.graphiti.features.context.IPasteContext;

/**
 * The Interface IPasteFeature.
 * 
 * @noextend This interface is not intended to be extended by clients.
 * @noimplement This interface is not intended to be implemented by clients,
 *              extend {@link AbstractPasteFeature} instead.
 */
public interface IPasteFeature extends IFeature {

	/**
	 * Paste.
	 * 
	 * @param context
	 *            the context
	 */
	void paste(IPasteContext context);

	/**
	 * Can paste.
	 * 
	 * @param context
	 *            the context
	 * 
	 * @return true, if successful
	 */
	boolean canPaste(IPasteContext context);
}