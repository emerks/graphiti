/*********************************************************************
* Copyright (c) 2005, 2019 SAP SE
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Contributors:
*    SAP SE - initial API, implementation and documentation
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipse.graphiti.features;

/**
 * The Interface IReason.
 * 
 * @noimplement This interface is not intended to be implemented by clients.
 * @noextend This interface is not intended to be extended by clients.
 */
public interface IReason {

	/**
	 * Converts to boolean.
	 * 
	 * @return true, if to boolean
	 */
	boolean toBoolean();

	/**
	 * Gets the text.
	 * 
	 * @return the reason in words
	 */
	String getText();
}
