/*********************************************************************
* Copyright (c) 2005, 2019 SAP SE
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Contributors:
*    SAP SE - initial API, implementation and documentation
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipse.graphiti.features.context;

import org.eclipse.graphiti.mm.pictograms.Anchor;
import org.eclipse.graphiti.mm.pictograms.AnchorContainer;

/**
 * The Interface IMoveAnchorContext.
 * 
 * @noimplement This interface is not intended to be implemented by clients.
 * @noextend This interface is not intended to be extended by clients.
 */
public interface IMoveAnchorContext extends IMoveContext {

	/**
	 * Gets the anchor.
	 * 
	 * @return the anchor
	 */
	Anchor getAnchor();

	/**
	 * Gets the target container.
	 * 
	 * @return the target container
	 */
	AnchorContainer getTargetContainer();

	/**
	 * Gets the source container.
	 * 
	 * @return the source container
	 */
	AnchorContainer getSourceContainer();
}