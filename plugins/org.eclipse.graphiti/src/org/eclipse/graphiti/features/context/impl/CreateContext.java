/*********************************************************************
* Copyright (c) 2005, 2019 SAP SE
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Contributors:
*    SAP SE - initial API, implementation and documentation
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipse.graphiti.features.context.impl;

import org.eclipse.graphiti.features.context.ICreateContext;
import org.eclipse.graphiti.mm.pictograms.Connection;
import org.eclipse.graphiti.mm.pictograms.ContainerShape;

/**
 * The Class CreateContext.
 */
public class CreateContext extends AreaContext implements ICreateContext {

	private ContainerShape targetContainer;

	private Connection targetConnection;

	/**
	 * Creates a new {@link CreateContext}.
	 */
	public CreateContext() {
		super();
	}

	public Connection getTargetConnection() {
		return this.targetConnection;
	}

	public ContainerShape getTargetContainer() {
		return this.targetContainer;
	}

	/**
	 * Sets the target connection.
	 * 
	 * @param targetConnection
	 *            The target connection to set.
	 */
	public void setTargetConnection(Connection targetConnection) {
		this.targetConnection = targetConnection;
	}

	/**
	 * Sets the target container.
	 * 
	 * @param targetContainer
	 *            The targetContainer to set.
	 */
	public void setTargetContainer(ContainerShape targetContainer) {
		this.targetContainer = targetContainer;
	}

	@Override
	public String toString() {
		String ret = super.toString();
		return ret + " targetConnection: " + getTargetConnection() + " targetContainer: " + getTargetContainer(); //$NON-NLS-1$ //$NON-NLS-2$
	}

}
