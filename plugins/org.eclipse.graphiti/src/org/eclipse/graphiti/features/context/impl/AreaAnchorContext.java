/*********************************************************************
* Copyright (c) 2005, 2019 SAP SE
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Contributors:
*    SAP SE - initial API, implementation and documentation
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipse.graphiti.features.context.impl;

import org.eclipse.graphiti.features.context.IMoveAnchorContext;
import org.eclipse.graphiti.mm.pictograms.Anchor;
import org.eclipse.graphiti.mm.pictograms.AnchorContainer;

/**
 * The Class AreaAnchorContext.
 */
public class AreaAnchorContext extends AreaContext implements IMoveAnchorContext {

	private Anchor anchor;

	private AnchorContainer sourceContainer;

	private AnchorContainer targetContainer;

	/**
	 * Creates a new {@link AreaAnchorContext}.
	 * 
	 * @param anchor
	 *            the anchor
	 */
	public AreaAnchorContext(Anchor anchor) {
		super();
		setAnchor(anchor);
	}

	/**
	 * Gets the anchor.
	 * 
	 * @return Returns the anchor.
	 */
	public Anchor getAnchor() {
		return this.anchor;
	}

	public AnchorContainer getSourceContainer() {
		return this.sourceContainer;
	}

	public AnchorContainer getTargetContainer() {
		return this.targetContainer;
	}

	/**
	 * Sets the source container.
	 * 
	 * @param sourceContainer
	 *            The sourceContainer to set.
	 */
	public void setSourceContainer(AnchorContainer sourceContainer) {
		this.sourceContainer = sourceContainer;
	}

	/**
	 * Sets the target container.
	 * 
	 * @param targetContainer
	 *            The targetContainer to set.
	 */
	public void setTargetContainer(AnchorContainer targetContainer) {
		this.targetContainer = targetContainer;
	}

	/**
	 * @param anchor
	 *            The anchor to set.
	 */
	private void setAnchor(Anchor anchor) {
		this.anchor = anchor;
	}

	@Override
	public String toString() {
		String ret = super.toString();
		return ret + " anchor: " + getAnchor() + " sourceContainer: " + getSourceContainer() + " targetContainer: " + getTargetContainer(); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
	}

}
