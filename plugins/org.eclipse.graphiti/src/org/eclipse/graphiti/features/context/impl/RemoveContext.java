/*********************************************************************
* Copyright (c) 2005, 2019 SAP SE
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Contributors:
*    SAP SE - initial API, implementation and documentation
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipse.graphiti.features.context.impl;

import org.eclipse.graphiti.features.context.IRemoveContext;
import org.eclipse.graphiti.internal.features.context.impl.base.PictogramElementContext;
import org.eclipse.graphiti.mm.pictograms.PictogramElement;

/**
 * The Class RemoveContext.
 */
public class RemoveContext extends PictogramElementContext implements IRemoveContext {

	/**
	 * Creates a new {@link RemoveContext}.
	 * 
	 * @param pictogramElement
	 *            the pictogram element
	 */
	public RemoveContext(PictogramElement pictogramElement) {
		super(pictogramElement);
	}
}