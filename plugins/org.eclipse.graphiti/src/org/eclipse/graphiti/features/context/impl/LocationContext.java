/*********************************************************************
* Copyright (c) 2005, 2019 SAP SE
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Contributors:
*    SAP SE - initial API, implementation and documentation
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipse.graphiti.features.context.impl;

import org.eclipse.graphiti.features.context.ILocationContext;
import org.eclipse.graphiti.internal.features.context.impl.base.DefaultContext;

/**
 * The Class LocationContext.
 */
public class LocationContext extends DefaultContext implements ILocationContext {

	private int x = -1;

	private int y = -1;

	/**
	 * Creates a new {@link LocationContext}.
	 * 
	 * @param x
	 *            the x
	 * @param y
	 *            the y
	 */
	public LocationContext(int x, int y) {
		super();
		setLocation(x, y);
	}

	/**
	 * Creates a new {@link LocationContext}.
	 */
	public LocationContext() {
	}

	public int getX() {
		return this.x;
	}

	public int getY() {
		return this.y;
	}

	/**
	 * Sets the location.
	 * 
	 * @param x
	 *            the x
	 * @param y
	 *            the y
	 */
	public void setLocation(int x, int y) {
		this.x = x;
		this.y = y;
	}

	/**
	 * Sets the x.
	 * 
	 * @param x
	 *            The x to set.
	 */
	public void setX(int x) {
		this.x = x;
	}

	/**
	 * Sets the y.
	 * 
	 * @param y
	 *            The y to set.
	 */
	public void setY(int y) {
		this.y = y;
	}

	@Override
	public String toString() {
		String ret = super.toString();
		ret = ret + "(x=" + getX() + ", y=" + getY() + ")"; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
		return ret;
	}
}
