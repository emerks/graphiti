/*********************************************************************
* Copyright (c) 2005, 2019 SAP SE
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Contributors:
*    SAP SE - initial API, implementation and documentation
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipse.graphiti.internal.util;

import org.eclipse.graphiti.internal.GraphitiPlugin;

/**
 * This class inherits from
 * <code>org.eclipse.graphiti.util.AbstractTracer</code>. Use it to add trace
 * output for the plugin <code>org.eclipse.graphiti</code>
 * 
 * @see org.eclipse.graphiti.internal.util.AbstractTracer
 * @noinstantiate This class is not intended to be instantiated by clients.
 * @noextend This class is not intended to be subclassed by clients.
 */
public final class T extends AbstractTracer {

	private static T t = new T(GraphitiPlugin.PLUGIN_ID);

	/**
	 * instantiate via <code>T.racer()</code>
	 */
	private T(String location) {
		super(location);
	}

	/**
	 * returns an instance of
	 * <code>org.eclipse.graphiti.util.AbstractTracer</code>
	 */
	public static T racer() {
		return t;
	}
}
