/*********************************************************************
* Copyright (c) 2005, 2019 SAP SE
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Contributors:
*    SAP SE - initial API, implementation and documentation
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipse.graphiti.internal.command;

import org.eclipse.graphiti.features.IFeature;
import org.eclipse.graphiti.features.context.IContext;

/**
 * The Class GenericFeatureCommandWithContext.
 * 
 * @noinstantiate This class is not intended to be instantiated by clients.
 * @noextend This class is not intended to be subclassed by clients.
 */
public class GenericFeatureCommandWithContext extends FeatureCommandWithContext {

	/**
	 * The Constructor.
	 * 
	 * @param context
	 *            the context
	 * @param feature
	 *            the feature
	 */
	public GenericFeatureCommandWithContext(IFeature feature, IContext context) {
		super(feature, context);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.graphiti.internal.command.ICommand#canExecute()
	 */
	public boolean canExecute() {
		boolean ret = true;
		IFeature f = getFeature();
		IContext c = getContext();
		ret = ret && f != null && c != null && f.canExecute(c);
		return ret;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.graphiti.internal.command.ICommand#execute()
	 */
	public boolean execute() {
		if (getContext() != null && getFeature() != null && getFeature().canExecute(getContext())) {
			getFeature().execute(getContext());
			return true;
		} else {
			return false;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.graphiti.internal.command.FeatureCommandWithContext#canUndo()
	 */
	@Override
	public boolean canUndo() {
		boolean ret = false;
		IFeature f = getFeature();
		IContext c = getContext();
		ret = f != null && f.canUndo(c);
		return ret;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.graphiti.internal.command.FeatureCommandWithContext#undo()
	 */
	@Override
	public boolean undo() {
		boolean ret = false;
		// IFeature f = getFeature();
		// IContext c = getContext();
		// ret = f != null && f.undo(c);
		return ret;
	}
}