/*********************************************************************
* Copyright (c) 2005, 2019 SAP SE
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Contributors:
*    SAP SE - initial API, implementation and documentation
*    mgorning - Bug 386913 - Support also Single-Click-Features
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipse.graphiti.internal.features.context.impl.base;

import org.eclipse.graphiti.features.context.IDoubleClickContext;
import org.eclipse.graphiti.mm.algorithms.GraphicsAlgorithm;
import org.eclipse.graphiti.mm.pictograms.PictogramElement;

/**
 * The Class DoubleClickContext.
 * 
 * @noinstantiate This class is not intended to be instantiated by clients.
 * @noextend This class is not intended to be subclassed by clients.
 */
public class DoubleClickContext extends AbstractClickContext implements IDoubleClickContext {

	/**
	 * Instantiates a new double click context.
	 * 
	 * @param pictogramElement
	 *            the pictogram element
	 * @param innerPictogramElement
	 *            the inner pictogram element
	 * @param innerGraphicsAlgorithm
	 *            the inner graphics algorithm
	 */
	public DoubleClickContext(PictogramElement pictogramElement, PictogramElement innerPictogramElement,
			GraphicsAlgorithm innerGraphicsAlgorithm) {
		super(pictogramElement, innerPictogramElement, innerGraphicsAlgorithm);
	}

}
