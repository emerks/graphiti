/*********************************************************************
* Copyright (c) 2005, 2019 SAP SE
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Contributors:
*    SAP SE - initial API, implementation and documentation
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipse.graphiti.platform.ga;

import org.eclipse.graphiti.dt.IDiagramTypeProvider;

/**
 * The Interface IGraphicsAlgorithmRenderer.
 * 
 * A marker interface enabling the framework to distinguish between customer
 * draw 2d figures and draw 2d figures created by the framework based on the
 * Graphiti model. If a user of Graphiti needs to display custom draw 2d figures
 * these figures need to implement this interface.
 * 
 * @see IGraphicsAlgorithmRendererFactory
 * @see IDiagramTypeProvider#getGraphicsAlgorithmRendererFactory
 */
public interface IGraphicsAlgorithmRenderer {
}
