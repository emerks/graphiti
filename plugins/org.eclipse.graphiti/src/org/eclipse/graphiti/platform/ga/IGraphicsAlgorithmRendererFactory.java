/*********************************************************************
* Copyright (c) 2005, 2019 SAP SE
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Contributors:
*    SAP SE - initial API, implementation and documentation
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipse.graphiti.platform.ga;

/**
 * The Interface IGraphicsAlgorithmRendererFactory.
 * 
 * A factory for creating IGraphicsAlgorithmRenderer objects.
 */
public interface IGraphicsAlgorithmRendererFactory {

	/**
	 * Creates a new IGraphicsAlgorithmRenderer object.
	 * 
	 * @param rendererContext
	 *            the renderer context
	 * 
	 * @return the IGraphics algorithm renderer
	 */
	IGraphicsAlgorithmRenderer createGraphicsAlgorithmRenderer(IRendererContext rendererContext);
}
