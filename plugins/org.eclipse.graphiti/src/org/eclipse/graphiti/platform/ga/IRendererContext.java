/*********************************************************************
* Copyright (c) 2005, 2019 SAP SE
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Contributors:
*    SAP SE - initial API, implementation and documentation
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipse.graphiti.platform.ga;

import org.eclipse.graphiti.IGraphicsAlgorithmHolder;
import org.eclipse.graphiti.dt.IDiagramTypeProviderHolder;
import org.eclipse.graphiti.features.IMappingProvider;
import org.eclipse.graphiti.mm.algorithms.PlatformGraphicsAlgorithm;

/**
 * The Interface IRendererContext.
 */
public interface IRendererContext extends IDiagramTypeProviderHolder, IGraphicsAlgorithmHolder {

	/**
	 * Gets the mapping provider.
	 * 
	 * @return the mapping provider
	 */
	IMappingProvider getMappingProvider();

	/**
	 * Gets the platform graphics algorithm.
	 * 
	 * @return the platform graphics algorithm
	 */
	PlatformGraphicsAlgorithm getPlatformGraphicsAlgorithm();
}
