/*********************************************************************
* Copyright (c) 2005, 2019 SAP SE
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Contributors:
*    SAP SE - initial API, implementation and documentation
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipse.graphiti.notification;

import org.eclipse.graphiti.mm.pictograms.PictogramElement;

/**
 * The Interface INotificationService.
 */
public interface INotificationService {

	/**
	 * Calculate dirty pictogram elements.
	 * 
	 * @param bos
	 *            the changed business objects
	 * @return the pictogram element[]
	 */
	PictogramElement[] calculateRelatedPictogramElements(Object[] bos);

	/**
	 * Update dirty pictogram elements.
	 * 
	 * @param pes
	 *            the dirty pes
	 */
	void updatePictogramElements(PictogramElement[] pes);

}
