/*********************************************************************
* Copyright (c) 2012, 2019 SAP SE
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Contributors:
*    SAP SE - initial API, implementation and documentation
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipse.graphiti.tools.newprojectwizard.internal;

import org.eclipse.osgi.util.NLS;

public class Messages extends NLS {
	private static final String BUNDLE_NAME = "org.eclipse.graphiti.tools.newprojectwizard.internal.messages"; //$NON-NLS-1$
	public static String GraphitiEditorTemplateSection_fieldNameClassName;
	public static String GraphitiEditorTemplateSection_fieldNameConnectionDomainObject;
	public static String GraphitiEditorTemplateSection_fieldNameDescription;
	public static String GraphitiEditorTemplateSection_fieldNameId;
	public static String GraphitiEditorTemplateSection_fieldNameName;
	public static String GraphitiEditorTemplateSection_fieldNamePackageName;
	public static String GraphitiEditorTemplateSection_fieldNameShapeDomainObject;
	public static String GraphitiEditorTemplateSection_fieldNameType;
	public static String GraphitiEditorTemplateSection_fieldNameUseConenctionDomainObject;
	public static String GraphitiEditorTemplateSection_fieldNameUsePatterns;
	public static String GraphitiEditorTemplateSection_fieldNameUseShapeDomainObject;
	public static String GraphitiEditorTemplateSection_groupNameConnectionDomainObject;
	public static String GraphitiEditorTemplateSection_groupNameDiagramType;
	public static String GraphitiEditorTemplateSection_groupNameDiagramTypeProvider;
	public static String GraphitiEditorTemplateSection_groupNameFeatureProvider;
	public static String GraphitiEditorTemplateSection_groupNameShapeDomainObject;
	public static String GraphitiEditorTemplateSection_pageDescription;
	public static String GraphitiEditorTemplateSection_pageDescriptionDomainObjects;
	public static String GraphitiEditorTemplateSection_pageName;
	public static String GraphitiEditorTemplateSection_pageTitleDomainObjects;
	public static String NewGraphitiEditorWizard_windowTitle;
	public static String SelectTypeOption_BrowseButton;
	public static String SelectTypeOption_DescriptionSelectDomainObject;
	public static String SelectTypeOption_TitleSelectDomainObject;
	static {
		// initialize resource bundle
		NLS.initializeMessages(BUNDLE_NAME, Messages.class);
	}

	private Messages() {
	}
}
