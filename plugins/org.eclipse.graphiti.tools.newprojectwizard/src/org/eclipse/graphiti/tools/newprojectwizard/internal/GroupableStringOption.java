/*********************************************************************
* Copyright (c) 2012, 2019 SAP SE
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Contributors:
*    SAP SE - initial API, implementation and documentation
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipse.graphiti.tools.newprojectwizard.internal;

import org.eclipse.pde.ui.templates.BaseOptionTemplateSection;
import org.eclipse.pde.ui.templates.StringOption;
import org.eclipse.swt.widgets.Composite;

/**
 * A string option that can be added to a group
 */
public class GroupableStringOption extends StringOption {

	private GroupOption groupOption;

	public GroupableStringOption(BaseOptionTemplateSection section, String name, String label, GroupOption groupOption) {
		super(section, name, label);
		this.groupOption = groupOption;
	}

	@Override
	public void createControl(Composite parent, int span) {
		super.createControl(groupOption.getGroup(), span);
	}
}
