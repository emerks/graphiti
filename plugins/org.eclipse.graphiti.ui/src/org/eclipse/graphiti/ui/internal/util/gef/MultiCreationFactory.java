/*********************************************************************
* Copyright (c) 2005, 2019 SAP SE
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Contributors:
*    SAP SE - initial API, implementation and documentation
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipse.graphiti.ui.internal.util.gef;

import java.util.List;

import org.eclipse.gef.requests.CreationFactory;
import org.eclipse.graphiti.features.IFeature;

/**
 * @noinstantiate This class is not intended to be instantiated by clients.
 * @noextend This class is not intended to be subclassed by clients.
 */
public class MultiCreationFactory implements CreationFactory {
	private List<IFeature> features;

	public MultiCreationFactory(List<IFeature> features) {
		this.features = features;
	}

	public Object getNewObject() {
		return features;
	}

	public Object getObjectType() {
		return null;
	}

}
