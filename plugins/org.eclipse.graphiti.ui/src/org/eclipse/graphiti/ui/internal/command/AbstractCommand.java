/*********************************************************************
* Copyright (c) 2005, 2019 SAP SE
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Contributors:
*    SAP SE - initial API, implementation and documentation
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipse.graphiti.ui.internal.command;

import org.eclipse.emf.transaction.TransactionalEditingDomain;
import org.eclipse.gef.commands.Command;
import org.eclipse.graphiti.features.IFeatureProvider;
import org.eclipse.graphiti.ui.internal.config.IConfigurationProviderHolder;
import org.eclipse.graphiti.ui.platform.IConfigurationProvider;

/**
 * @noinstantiate This class is not intended to be instantiated by clients.
 * @noextend This class is not intended to be subclassed by clients.
 */
public abstract class AbstractCommand extends Command implements IConfigurationProviderHolder {

	private IConfigurationProvider configurationProvider;

	public AbstractCommand(IConfigurationProvider configurationProvider, String label) {
		super(label);
		this.configurationProvider = configurationProvider;
	}

	public AbstractCommand(IConfigurationProvider configurationProvider) {
		super();
		this.configurationProvider = configurationProvider;
	}

	public IConfigurationProvider getConfigurationProvider() throws IllegalStateException {
		return configurationProvider;
	}

	protected IFeatureProvider getFeatureProvider() {
		return getConfigurationProvider().getDiagramTypeProvider().getFeatureProvider();
	}

	protected TransactionalEditingDomain getTransactionalEditingDomain() {
		return configurationProvider.getDiagramBehavior().getEditingDomain();
	}
}