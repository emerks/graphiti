/*********************************************************************
* Copyright (c) 2005, 2019 SAP SE
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Contributors:
*    SAP SE - initial API, implementation and documentation
*    mwenz - Bug 459386 - Refresh Connection when getDiagramBehavior().refreshRenderingDecorators(PEInstance) is called
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipse.graphiti.ui.internal.parts;

import org.eclipse.gef.EditPart;

/**
 * Window - Preferences - Java - Code Style - Code Templates.
 * 
 * @noimplement This interface is not intended to be implemented by clients.
 * @noextend This class is not intended to be subclassed by clients.
 */
public interface IShapeEditPart extends IAnchorContainerEditPart {

	/**
	 * Delete child and refresh.
	 * 
	 * @param childEditPart
	 *            the child edit part
	 */
	void deleteChildAndRefresh(EditPart childEditPart);
}