/*********************************************************************
* Copyright (c) 2005, 2019 SAP SE
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Contributors:
*    SAP SE - initial API, implementation and documentation
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipse.graphiti.ui.internal.command;

import org.eclipse.graphiti.features.IFeature;
import org.eclipse.graphiti.features.context.IContext;
import org.eclipse.graphiti.ui.platform.IConfigurationProvider;

/**
 * @noinstantiate This class is not intended to be instantiated by clients.
 * @noextend This class is not intended to be subclassed by clients.
 */
public class GFCommand extends AbstractCommand {

	private IContext context = null;

	private IFeature feature = null;

	public GFCommand(IConfigurationProvider configurationProvider) {
		super(configurationProvider);
	}

	public GFCommand(IConfigurationProvider configurationProvider, String label) {
		super(configurationProvider, label);
	}

	public IContext getContext() {
		return context;
	}

	public IFeature getFeature() {
		return feature;
	}

	public void setFeature(IFeature feature) {
		this.feature = feature;
	}

	public void setContext(IContext context) {
		this.context = context;
	}

}
