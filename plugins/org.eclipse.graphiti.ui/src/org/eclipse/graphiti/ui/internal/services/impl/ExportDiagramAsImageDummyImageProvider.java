/*********************************************************************
* Copyright (c) 2015, 2019 Eclipse Modeling Project.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Contributors:
*    Laurent Le Moux, mwenz - Bug 423018 - Direct Graphiti diagram exporter
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipse.graphiti.ui.internal.services.impl;

import org.eclipse.graphiti.platform.AbstractExtension;
import org.eclipse.graphiti.ui.platform.IImageProvider;

public class ExportDiagramAsImageDummyImageProvider extends AbstractExtension implements IImageProvider {

	private String pluginId;

	public ExportDiagramAsImageDummyImageProvider() {
		super();
	}

	final public String getPluginId() {
		return this.pluginId;
	}

	final public void setPluginId(String pluginId) {
		this.pluginId = pluginId;
	}

	final public String getImageFilePath(String imageId) {
		return "icons/NoImage.png"; //$NON-NLS-1$
	}
}
