/*********************************************************************
* Copyright (c) 2005, 2019 SAP SE
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Contributors:
*    SAP SE - initial API, implementation and documentation
*    mwenz - Bug 336075 - DiagramEditor accepts URIEditorInput
*    mwenz - Bug 346932 - Navigation history broken
*    Bug 336488 - DiagramEditor API
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipse.graphiti.ui.editor;

import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.emf.common.util.URI;
import org.eclipse.ui.IElementFactory;
import org.eclipse.ui.IMemento;

/**
 * The Class DiagramEditorFactory.
 * 
 * A factory for creating DiagramEditorInput objects.
 * 
 * @see {@link DiagramEditorInputFactory}
 * @see {@link DiagramEditorInput}
 * @since 0.9 results from renaming DiagramEditorFactory
 */
public class DiagramEditorInputFactory implements IElementFactory {

	public IAdaptable createElement(IMemento memento) {
		// get diagram URI
		final String diagramUriString = memento.getString(DiagramEditorInput.KEY_URI);
		if (diagramUriString == null) {
			return null;
		}
		// get diagram type provider id
		final String providerID = memento.getString(DiagramEditorInput.KEY_PROVIDER_ID);
		URI diagramUri = URI.createURI(diagramUriString);
		return new DiagramEditorInput(diagramUri, providerID);
	}
}