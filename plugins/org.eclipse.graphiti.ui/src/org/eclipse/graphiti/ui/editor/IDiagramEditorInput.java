/*********************************************************************
* Copyright (c) 2005, 2019 SAP SE
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Contributors:
*    Bug 336488 - DiagramEditor API
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipse.graphiti.ui.editor;

import org.eclipse.emf.common.util.URI;

/**
 * @since 0.9
 */
public interface IDiagramEditorInput {

	String getUriString();

	URI getUri();

	String getProviderId();

	void setProviderId(String providerId);

	void updateUri(URI newURI);

}
