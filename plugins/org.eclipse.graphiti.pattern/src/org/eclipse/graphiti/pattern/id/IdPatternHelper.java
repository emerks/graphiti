/*********************************************************************
* Copyright (c) 2021, 2021 SAP SE
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Contributors:
*    SAP SE - initial API, implementation and documentation
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipse.graphiti.pattern.id;

import org.eclipse.emf.common.util.EList;
import org.eclipse.graphiti.mm.Property;
import org.eclipse.graphiti.mm.PropertyContainer;
import org.eclipse.graphiti.mm.algorithms.GraphicsAlgorithm;
import org.eclipse.graphiti.mm.pictograms.ContainerShape;
import org.eclipse.graphiti.mm.pictograms.PictogramElement;
import org.eclipse.graphiti.mm.pictograms.Shape;
import org.eclipse.graphiti.services.Graphiti;

/**
 * Some helper methods to be used in ID patterns and related features.
 * 
 * @since 0.18
 */
public class IdPatternHelper {

	/**
	 * Returns any ID that has been set for the given {@link PictogramElement};
	 * it can be any {@link PropertyContainer}, especially {@link Shape}s or
	 * {@link GraphicsAlgorithm}s are allowed.
	 * 
	 * @param container
	 *            The {@link PictogramElement} to get the ID property from
	 * @return A {@link String} representing the value of the property or
	 *         <code>null</code> in case the property is not set, see
	 *         {@link #setId(PropertyContainer, String)}.
	 */
	public static String getId(PropertyContainer container) {
		EList<Property> properties = container.getProperties();
		for (Property property : properties) {
			if (IdPattern.PROPERTY_KEY_ID.equals(property.getKey())) {
				return property.getValue();
			}
		}
		return null;
	}

	/**
	 * Set the ID property ({@link #PROPERTY_KEY_ID}) for the given
	 * {@link PictogramElement}; it can be any {@link PropertyContainer}
	 * ,especially {@link Shape}s or {@link GraphicsAlgorithm}s are allowed. The
	 * used ID string can later be used to identify the shape, e.g. in the
	 * update or layout methods.
	 * 
	 * @param container
	 *            The {@link PictogramElement} to set the ID property for
	 * @param id
	 *            The {@link String} ID to set.
	 */
	public static void setId(PropertyContainer container, String id) {
		Graphiti.getPeService().setPropertyValue(container, IdPattern.PROPERTY_KEY_ID, id);
	}

	/**
	 * Searches for a {@link PictogramElement} that has the given ID starting
	 * from the given {@link PictogramElement}. First the given element is
	 * checked, then its {@link GraphicsAlgorithm}; after that the
	 * {@link PictogramElement} children are checked recursively and last the
	 * {@link GraphicsAlgorithm} children also recursively. The first
	 * {@link PictogramElement} that has the given ID is returned, in case none
	 * is found in the tree spanned by the given {@link PictogramElement},
	 * <code>null</code> is returned.
	 * 
	 * @param pictogramElement
	 *            The {@link PictogramElement} at which the search shall start,
	 *            any {@link Shape}s or {@link GraphicsAlgorithm}s on top of
	 *            this element are ignored.
	 * @param idToFind
	 *            A {@link String} representing the ID to search for
	 * @return The {@link PictogramElement} that has the given ID property, in
	 *         case none id found <code>null</code>.
	 */
	public static PropertyContainer findById(PictogramElement pictogramElement, String idToFind) {
		if (idToFind == null || idToFind.length() == 0) {
			return null;
		}

		// Check id for PE
		String id = getId(pictogramElement);
		if (idToFind.equals(id)) {
			return pictogramElement;
		}

		// Check id for GA
		GraphicsAlgorithm graphicsAlgorithm = pictogramElement.getGraphicsAlgorithm();
		id = getId(graphicsAlgorithm);
		if (idToFind.equals(id)) {
			return graphicsAlgorithm;
		}

		// Check children of PE
		if (pictogramElement instanceof ContainerShape) {
			EList<Shape> children = ((ContainerShape) pictogramElement).getChildren();
			for (Shape shape : children) {
				PropertyContainer propertyContainer = findById(shape, idToFind);
				if (propertyContainer != null) {
					return propertyContainer;
				}
			}
		}

		// Check children of GA
		PropertyContainer propertyContainer = findByIdInGraphicsAlgorithmChildren(graphicsAlgorithm, idToFind);
		if (propertyContainer != null) {
			return propertyContainer;
		}

		return null;
	}

	/**
	 * Searches for a {@link PictogramElement} that has the given ID starting
	 * from the given {@link PictogramElement}. The {@link PictogramElement}
	 * children are checked recursively and last the {@link GraphicsAlgorithm}
	 * children also recursively. The first {@link PictogramElement} that has
	 * the given ID is returned, in case none is found in the tree spanned by
	 * the given {@link PictogramElement}, <code>null</code> is returned.
	 * 
	 * @param pictogramElement
	 *            The {@link PictogramElement} at which the search shall start,
	 *            any {@link Shape}s or {@link GraphicsAlgorithm}s on top of
	 *            this element are ignored.
	 * @param idToFind
	 *            A {@link String} representing the ID to search for
	 * @return The {@link PictogramElement} that has the given ID property, in
	 *         case none id found <code>null</code>.
	 */
	public static PropertyContainer findByIdInGraphicsAlgorithmChildren(GraphicsAlgorithm graphicsAlgorithm,
			String idToFind) {
		EList<GraphicsAlgorithm> graphicsAlgorithmChildren = graphicsAlgorithm.getGraphicsAlgorithmChildren();
		for (GraphicsAlgorithm graphicsAlgorithmChild : graphicsAlgorithmChildren) {
			String id = getId(graphicsAlgorithmChild);
			if (idToFind.equals(id)) {
				return graphicsAlgorithmChild;
			}

			PropertyContainer propertyContainer = findByIdInGraphicsAlgorithmChildren(graphicsAlgorithmChild, idToFind);
			if (propertyContainer != null) {
				return propertyContainer;
			}
		}
		return null;
	}
}
