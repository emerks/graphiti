/*********************************************************************
* Copyright (c) 2005, 2019 SAP SE
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Contributors:
*    SAP SE - initial API, implementation and documentation
*    mwenz - Bug 325084 - Provide documentation for Patterns
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipse.graphiti.pattern.mapping.data;

import org.eclipse.graphiti.mm.pictograms.PictogramLink;

/**
 * The Interface IDataMapping.
 * 
 * @experimental This API is in an experimental state and should be used by
 *               clients, as it not final and can be removed or changed without
 *               prior notice!
 * 
 * @noimplement This interface is not intended to be implemented by clients.
 * @noextend This interface is not intended to be extended by clients. Extend
 *           {@link DataMapping} instead
 */
public interface IDataMapping {

	/**
	 * Gets the update warning.
	 * 
	 * @param pictogramLink
	 *            the pictogram link
	 * 
	 * @return the update warning
	 */
	String getUpdateWarning(PictogramLink pictogramLink);
}
