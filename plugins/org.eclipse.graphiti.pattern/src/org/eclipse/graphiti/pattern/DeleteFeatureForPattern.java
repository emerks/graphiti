/*********************************************************************
* Copyright (c) 2012, 2019 Volker Wegert and others.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Contributors:
*    Volker Wegert - initial API, implementation and documentation:
*                    Bug 336828: patterns should support delete,
*                    remove, direct editing and conditional palette
*                    creation entry
*    mwenz - Bug 324859 - Need Undo/Redo support for Non-EMF based domain objects
*    mwenz - Bug 325084 - Provide documentation for Patterns
*    mwenz - Bug 443304 - Improve undo/redo handling in Graphiti features
*    mwenz - Bug 453553 - Provide an abort possibility for delete and remove features in case 'pre' methods fail
*    mwenz - Bug 481994 - Some XxxFeatureForPattern classes call ICustomUndoablePattern#redo instead of ICustomUndoRedoPattern#postRedo
*    mwenz - Bug 472955 - Remove ICustomUndoableFeature and ICustomUndoablePattern deprecated in Graphiti 0.12.0
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipse.graphiti.pattern;

import org.eclipse.graphiti.features.ICustomAbortableUndoRedoFeature;
import org.eclipse.graphiti.features.IFeatureProvider;
import org.eclipse.graphiti.features.context.IContext;
import org.eclipse.graphiti.features.context.IDeleteContext;
import org.eclipse.graphiti.func.IDelete;
import org.eclipse.graphiti.ui.features.DefaultDeleteFeature;

/**
 * This feature wraps the delete functionality of a pattern for calls of the
 * Graphiti framework. Clients should not need to use this class directly.
 * 
 * @noextend This class is not intended to be subclassed by clients.
 * @noinstantiate This class is not intended to be instantiated by clients.
 * 
 * @since 0.8.0
 */
public class DeleteFeatureForPattern extends DefaultDeleteFeature implements ICustomAbortableUndoRedoFeature {

	private IFeatureForPattern delegate;

	/**
	 * Creates a new {@link DeleteFeatureForPattern}.
	 * 
	 * @param featureProvider
	 *            the feature provider
	 * @param pattern
	 *            the pattern
	 */
	public DeleteFeatureForPattern(IFeatureProvider featureProvider, IPattern pattern) {
		super(featureProvider);
		delegate = new FeatureForPatternDelegate(pattern);
	}

	@Override
	public boolean canDelete(IDeleteContext context) {
		return delegate.getPattern().canDelete(context);
	}

	@Override
	public void preDelete(IDeleteContext context) {
		delegate.getPattern().preDelete(context);
	}

	/**
	 * @since 0.12
	 */
	@Override
	public boolean isDeleteAbort() {
		IPattern pattern = delegate.getPattern();
		return pattern.isDeleteAbort();
	}

	@Override
	public void delete(IDeleteContext context) {
		delegate.getPattern().delete(context);
	}

	@Override
	public void postDelete(IDeleteContext context) {
		delegate.getPattern().postDelete(context);
	}

	/**
	 * @since 0.12
	 */
	@Override
	public boolean isAbort() {
		IPattern pattern = delegate.getPattern();
		if (pattern instanceof ICustomAbortableUndoRedoPattern) {
			return ((ICustomAbortableUndoRedoPattern) pattern).isAbort();
		}
		return false;
	}

	@Override
	public boolean canUndo(IContext context) {
		IPattern pattern = delegate.getPattern();
		if (pattern instanceof ICustomUndoRedoPattern) {
			return ((ICustomUndoRedoPattern) pattern).canUndo(this, context);
		}
		return super.canUndo(context);
	}

	/**
	 * @since 0.12
	 */
	@Override
	public void preUndo(IContext context) {
		IPattern pattern = delegate.getPattern();
		if (pattern instanceof ICustomUndoRedoPattern) {
			((ICustomUndoRedoPattern) pattern).preUndo(this, context);
		}
	}

	/**
	 * @since 0.12
	 */
	@Override
	public void postUndo(IContext context) {
		IPattern pattern = delegate.getPattern();
		if (pattern instanceof ICustomUndoRedoPattern) {
			((ICustomUndoRedoPattern) pattern).postUndo(this, context);
		}
	}

	public boolean canRedo(IContext context) {
		IPattern pattern = delegate.getPattern();
		if (pattern instanceof ICustomUndoRedoPattern) {
			return ((ICustomUndoRedoPattern) pattern).canRedo(this, context);
		}
		return true;
	}

	/**
	 * @since 0.12
	 */
	@Override
	public void preRedo(IContext context) {
		IPattern pattern = delegate.getPattern();
		if (pattern instanceof ICustomUndoRedoPattern) {
			((ICustomUndoRedoPattern) pattern).preRedo(this, context);
		}
	}

	/**
	 * @since 0.12
	 */
	@Override
	public void postRedo(IContext context) {
		IPattern pattern = delegate.getPattern();
		if (pattern instanceof ICustomUndoRedoPattern) {
			((ICustomUndoRedoPattern) pattern).postRedo(this, context);
		}
	}

	@Override
	public boolean hasDoneChanges() {
		IPattern pattern = delegate.getPattern();
		return pattern.hasDoneChanges(IDelete.class);
	}
}
