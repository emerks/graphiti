# Security Policy

Eclipse Graphiti follows the [Eclipse Vulnerability Reporting Policy](https://www.eclipse.org/security/policy.php). Vulnerabilities are tracked by the Eclipse security team, in cooperation with the Graphiti project lead. Fixing vulnerabilities is taken care of by the Graphiti project committers, with assistance and guidance of the security team. 

## Supported Versions

Eclipse Graphiti supports security updates for the following releases:

| Version | Supported          |
| ------- | ------------------ |
| 0.19.0  | :white_check_mark: |
| 0.18.0  | :white_check_mark: |

## Reporting a Vulnerability

We recommend that in case of suspected vulnerabilities you do not use the Graphiti public issue tracker, but instead contact the Eclipse Security Team directly via security@eclipse.org.